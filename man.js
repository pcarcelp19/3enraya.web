var cells = document.getElementsByClassName("celda");

[].forEach.call(cells, function (el) {
  el.addEventListener("click", whenClick);
  el.addEventListener("dblclick", whenDoubleClick);
});

function whenClick() {
  this.innerHTML = "X";
}

function whenDoubleClick() {
  this.innerHTML = "O";
}
